const baseUrl = config.baseUrl;

const VoteList = Vue.extend({
    template: '#vote-list',
    data: function() {
        return {
            votes: [],
            fields: [
                { key: 'title', label: 'Title', sortable: true  },
                { key: 'options', label: 'Options', sortable: true },
                { key: 'votes', label: 'Votes', 'class': 'text-center' },
                { key: 'actions', label: 'Actions' }
              ],
            currentPage: 1,
            perPage: config.pageSize,
            totalRows: 0,
            sortBy: null,
            sortDesc: false,
            filter: null,
            modalRemove: { title: '', content: '' }
        }
    },
    created() {
        fetch(baseUrl + '/polls/?key=abc123')
            .then(res => res.json())
            .then(json => {
                this.votes = json;
                this.totalRows = json.length;
                this.currentPage = 1;
            })
    },
    computed: {
        totalVotes() {
            return this.votes.length;
        } 
    },
    methods: {
        onFiltered(items) {
            this.totalRows = items.length;
            this.currentPage = 1;
        },
        calculateVotes(item) {
            if (item.results == null) {
                return 0;
            }
            return Object.values(item.results).reduce((a, b) => a + b, 0);
        },
        showModalRemove(item, index, button) {
            this.modalRemove.title = "Delete vote";
            this.modalRemove.content = item;
            this.$root.$emit('bv::show::modal', 'modalRemove', button)
        },
        hideModalRemove() {
            this.$root.$emit('bv::hide::modal', 'modalRemove')
        },
        removeVote(id) {
            fetch(baseUrl + '/polls/' + id + '?key=abc123', {
                method: 'DELETE'
            }).then(res => {
                if (res.status == 200) {
                    this.hideModalRemove();
                    this.votes = this.votes.filter(item => item.id != id);
                } else {
                    console.log('Wrong response status', res);
                }
              })
              .catch(function(err) {
                console.log('Fetch Error :-S', err);
              });
        },
    }
});

const VoteCreate = {
    template: '#vote-form',
    data: function() {
        return { 
            vote: { title: '', options: ''},
            buttonText: "Create"
        }
    },
    methods: {
        submit: function() {
            var vote = this.vote;
            fetch(baseUrl + '/polls/?key=abc123', {
                method: 'POST',
                body: JSON.stringify({ title: vote.title, options: vote.options.split(",").map(e=>e.trim()) })
            }).then(res => {
                console.log(res.status);
                if (res.status == 201) {
                    console.log("saved");
                    router.push('/');
                } else {
                    console.log('Wrong response status', res);
                }
              })
              .catch(function(err) {
                console.log('Fetch Error :-S', err);
              });
        }
    }
}

const VoteEdit = {
    template: '#vote-form',
    data: function() {
        return { 
            vote: { title: '', options: ''},
            buttonText: "Save"
        }
    },
    created() {
        this.loadData()
    },
    methods: {
        submit: function() {
            var vote = this.vote;
            fetch(baseUrl + '/polls/' + this.$route.params.id + '?key=abc123', {
                method: 'PUT',
                body: JSON.stringify({ title: vote.title, options: vote.options.split(",").map(e=>e.trim()) })
            }).then(res => {
                if (res.status == 201) {
                    router.push('/');
                } else {
                    console.log('Wrong response status', res);
                }
              })
              .catch(function(err) {
                console.log('Fetch Error :-S', err);
              });
        },
        loadData: function() {
            fetch(baseUrl + '/polls/' + this.$route.params.id + '?key=abc123')
                .then(res => res.json())
                .then(json => {
                    this.vote = json[0]
                    this.vote.options = this.vote.options.join(",")
                })
        }
    }
}

const VoteView = {
    template: '#vote-view',
    data: function() {
        return { vote: {results:[]} }
    },
    created() {
        this.loadData()
    },
    methods: {
        loadData: function() {
            let id = this.$route.params.id;
            if (id) {
                fetch(baseUrl + '/polls/' + this.$route.params.id + '?key=abc123')
                    .then(res => res.json())
                    .then(json => {
                        this.vote = json[0]
                        setTimeout(this.loadData, 1000);
                    })
            }
        }
    },
    computed: {
        orederedResults: function() {
            var sortable = [];
            for (var key in this.vote.results) {
                sortable.push({ name: key, count: this.vote.results[key]});
            }
            sortable.sort((a, b) => b.count - a.count);
            return sortable;
        }
    }
}

const About = { template: '#about' }

const router = new VueRouter({
    routes: [
        { path: '/', component: VoteList, name: 'index' },
        { path: '/about', component: About },
        { path: '/vote/create', component: VoteCreate, name: 'voteCreate' },
        { path: '/vote/edit/:id', component: VoteEdit, name: 'voteEdit' },
        { path: '/vote/:id', component: VoteView, name: 'vote' },
    ]
});

const app = new Vue({ router: router }).$mount('#app')
